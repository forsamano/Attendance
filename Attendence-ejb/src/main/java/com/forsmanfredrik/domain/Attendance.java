package com.forsmanfredrik.domain;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

/**
 * Created by ForsmanFredrik on 2015-10-01.
 */
@Entity
@NamedQueries({
        @NamedQuery(name = "attendance.findAll", query = "select a from Attendance a"),
        @NamedQuery(name = "attendance.findAttendanceByDate", query = "select a from Attendance a where a.selectedDate = :date")
})
public class Attendance implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToMany(fetch = FetchType.EAGER)
    private Set<Student> studentSet;

    @OneToOne(fetch = FetchType.EAGER)
    private Course course;
    @Temporal(TemporalType.DATE)
    @NotNull(message = "Select a date please")
    private Date selectedDate;

    public Attendance(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Set<Student> getStudentSet() {
        return studentSet;
    }

    public void setStudentSet(Set<Student> studentSet) {


        this.studentSet = studentSet;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Date getSelectedDate() {
        return selectedDate;
    }

    public void setSelectedDate(Date selectedDate) {
        this.selectedDate = selectedDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Attendance that = (Attendance) o;

        if (id != that.id) return false;
        if (studentSet != null ? !studentSet.equals(that.studentSet) : that.studentSet != null) return false;
        if (course != null ? !course.equals(that.course) : that.course != null) return false;
        return !(selectedDate != null ? !selectedDate.equals(that.selectedDate) : that.selectedDate != null);

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (studentSet != null ? studentSet.hashCode() : 0);
        result = 31 * result + (course != null ? course.hashCode() : 0);
        result = 31 * result + (selectedDate != null ? selectedDate.hashCode() : 0);
        return result;
    }

}
