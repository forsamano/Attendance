package com.forsmanfredrik.domain;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by ForsmanFredrik on 2015-09-14.
 */

@Entity
@NamedQueries({@NamedQuery(name = "Course.findAll", query = "select c from Course c"),
})
public class Course implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @NotEmpty(message = "You forgot the name of the Course")
    private String courseName;
    private int maxStudents;
    @NotEmpty(message = "You forgot the name of the Teacher")
    private String teacher;
    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH})
    @JoinTable(name = "course_students", joinColumns = @JoinColumn(name = "COURSE_ID", referencedColumnName = "id"),
            inverseJoinColumns =
            @JoinColumn(name = "STUDENT_ID", referencedColumnName = "id"))
    private Set<Student> studentList;

    public Course() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCourseName() {
        return courseName;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public int getMaxStudents() {
        return maxStudents;
    }

    public void setMaxStudents(int maxStudents) {
        this.maxStudents = maxStudents;
    }

    public Set<Student> getStudentList() {
        return studentList;
    }

    public void setStudentList(Set<Student> studentList) {
        this.studentList = studentList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Course course = (Course) o;

        if (id != course.id) return false;
        if (maxStudents != course.maxStudents) return false;
        if (!courseName.equals(course.courseName)) return false;
        return teacher.equals(course.teacher);

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + courseName.hashCode();
        result = 31 * result + maxStudents;
        result = 31 * result + teacher.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return String.format("%s, [%s, %s, %s]",
                Student.class.getSimpleName(), courseName, maxStudents, teacher);
    }
}
