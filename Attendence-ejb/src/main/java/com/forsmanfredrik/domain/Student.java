package com.forsmanfredrik.domain;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ForsmanFredrik on 2015-09-10.
 */

@Entity
@NamedQueries({
        @NamedQuery(name="Student.findAll", query = "select s from Student s"),
        @NamedQuery(name="Student.findStudentById", query = "Select s from Student s where s.id = :id"),
        })
public class Student implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @NotEmpty(message = "*")
    private String firstName;
    @NotEmpty(message = "*")
    private String lastName;
    @NotEmpty(message = "*")
    private String street;
    @NotEmpty(message = "*")
    private String city;
    private int zip;
    @NotEmpty(message = "*")
    private String phoneNumber;
    @Email @NotEmpty @NotNull
    private String eMail;
    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH})
    private List<Course> courseList;
    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "studentSet")
    private List<Attendance> attendances;

    public Student() {
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getZip() {
        return zip;
    }

    public void setZip(int zip) {
        this.zip= zip;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String geteMail() {
        return eMail;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail;
    }

    public List<Course> getCourseList() {
        return courseList;
    }

    public void setCourseList(List<Course> courseList) {
        this.courseList = courseList;
    }

    public List<Attendance> getAttendances() {
        return attendances;
    }

    public void setAttendances(List<Attendance> attendances) {
        this.attendances = attendances;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Student student = (Student) o;

        if (id != student.id) return false;
        if (zip != student.zip) return false;
        if (!firstName.equals(student.firstName)) return false;
        if (!lastName.equals(student.lastName)) return false;
        if (!street.equals(student.street)) return false;
        if (!city.equals(student.city)) return false;
        if (!phoneNumber.equals(student.phoneNumber)) return false;
        return eMail.equals(student.eMail);

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + firstName.hashCode();
        result = 31 * result + lastName.hashCode();
        result = 31 * result + street.hashCode();
        result = 31 * result + city.hashCode();
        result = 31 * result + zip;
        result = 31 * result + phoneNumber.hashCode();
        result = 31 * result + eMail.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return String.format("%s, [%s, %s, %s, %s, %s, %s, %s]",
                Student.class.getSimpleName(), firstName, lastName, street, city, zip, phoneNumber, eMail);
    }
}
