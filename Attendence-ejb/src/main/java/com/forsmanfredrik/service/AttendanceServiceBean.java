package com.forsmanfredrik.service;

import com.forsmanfredrik.domain.Attendance;
import com.forsmanfredrik.domain.Student;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Created by ForsmanFredrik on 2015-10-01.
 */
@Stateless(name = "AttendanceServiceEJB")
public class AttendanceServiceBean implements AttendanceServiceLocal {

    @PersistenceContext
    EntityManager em;

    @Inject
    AttendanceServiceLocal attendanceServiceLocal;

    /**
     * {@inheritDoc}
     *
     */
    @Override
    public void addAttendance(Attendance attendance){

        em.persist(attendance);
    }

    /**
     * {@inheritDoc}
     *
     */
    @Override
    public Attendance findAttendanceById(int id){
        try{
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Attendance> query = cb.createQuery(Attendance.class);
            Root<Attendance> attendance = query.from(Attendance.class);
            query.where(cb.equal(attendance.get("id"), id));
            return em.createQuery(query).getSingleResult();
        }catch (NoResultException nre) {
            nre.printStackTrace();
            return null;
        } catch (NonUniqueResultException nur) {
            nur.printStackTrace();
            return null;
        }
    }

    @Override
    public Set<Student> addStudentToAttendance(int id, Student student){
        Attendance attendance = findAttendanceById(id);

        Set<Student> students = attendance.getStudentSet();

        students.add(student);
        attendance.setStudentSet(students);
        em.merge(attendance);
        return students;
    }

    /**
     * {@inheritDoc}
     *
     */
    @Override
    public List<Attendance> getAttendances() {
        TypedQuery<Attendance> query = em.createNamedQuery("attendance.findAll", Attendance.class);
        List<Attendance> attendances = query.getResultList();
        return attendances;
    }

    /**
     * {@inheritDoc}
     *
     */
    @Override
    public List<Attendance> getAttendancesByDate(Date date) {
        TypedQuery<Attendance> query = em.createNamedQuery("attendance.findAttendanceByDate", Attendance.class).setParameter("date", date);
        List<Attendance> attendancesByDate = query.getResultList();
        return attendancesByDate;
    }

}
