package com.forsmanfredrik.service;

import com.forsmanfredrik.domain.Attendance;
import com.forsmanfredrik.domain.Course;
import com.forsmanfredrik.domain.Student;

import javax.ejb.Local;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Created by ForsmanFredrik on 2015-10-01.
 */
@Local
public interface AttendanceServiceLocal {

    /**
     * Adds a attendance to the database
     * @param attendance this attendance
     */
    void addAttendance(Attendance attendance);

    /**
     * Finds the attendance by it's id
     * @param id this attendance's id
     * @return this attendance
     */
    Attendance findAttendanceById(int id);

    Set<Student> addStudentToAttendance(int id, Student student);

    /**
     * Gets all the attendances in the database
     * @return all attendances
     */
    List<Attendance> getAttendances();

    /**
     * Gets all attendances in the database by the selected date
     * @param date the selected date
     * @return all attendances on the selected date
     */
    List<Attendance> getAttendancesByDate(Date date);
}
