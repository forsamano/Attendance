package com.forsmanfredrik.service;

import com.forsmanfredrik.domain.Course;
import com.forsmanfredrik.domain.Student;

import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.sql.SQLDataException;
import java.util.*;

/**
 * Created by ForsmanFredrik on 2015-09-14.
 */
@Stateless(name = "CourseServiceEJB")
public class CourseServiceBean implements CourseServiceLocal {

    @PersistenceContext
    EntityManager em;

    @Inject
    ServiceLocal serviceLocal;

    public CourseServiceBean() {
    }

    /**
     * {@inheritDoc}
     *
     */
    @Override
    public void addCourse(Course course) {

        em.persist(course);
        em.flush();
    }

    /**
     * {@inheritDoc}
     *
     */
    @Override
    public List<Course> getCourses() {
        TypedQuery<Course> query = em.createNamedQuery("Course.findAll", Course.class);
        List<Course> courses = query.getResultList();
        return courses;
    }

    /**
     * {@inheritDoc}
     *
     */
    @Override
    public Course findCourseById(int id) {
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Course> query = cb.createQuery(Course.class);
            Root<Course> course = query.from(Course.class);
            query.where(cb.equal(course.get("id"), id));

            return em.createQuery(query).getSingleResult();
        } catch (NoResultException nre) {
            nre.printStackTrace();
            return null;
        } catch (NonUniqueResultException nur) {
            nur.printStackTrace();
            return null;
        }
    }

    /**
     * {@inheritDoc}
     *
     */
    @Override
    public void removeCourse(int id) {
        Course course = findCourseById(id);
        em.remove(course);
    }

    /**
     * {@inheritDoc}
     *
     */
    @Override
    public Course editCourse(Course cour) {
        Course course = findCourseById(cour.getId());
        course.setCourseName(cour.getCourseName());
        course.setMaxStudents(cour.getMaxStudents());
        course.setTeacher(cour.getTeacher());

        return em.merge(course);
    }

    /**
     * {@inheritDoc}
     *
     */
    @Override
    public Set<Student> addStudentToCourse(int id, Student student) {

        Course course = findCourseById(id);
        Set<Student> students = course.getStudentList();

        students.add(student);
        course.setStudentList(students);
        em.merge(course);

        return students;
    }

    /**
     * {@inheritDoc}
     *
     */
    @Override
    public Set<Student> removeStudentFromCourse(int courseId, Student student) {
        Course course = findCourseById(courseId);
        Set<Student> students = course.getStudentList();

        students.remove(student);
        em.merge(course);

        return students;
    }

    /**
     * {@inheritDoc}
     *
     */
    @Override
    public Set<Student> getStudentsInCourse(int id) {

        try {
            Course course = findCourseById(id);
            return course.getStudentList();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
