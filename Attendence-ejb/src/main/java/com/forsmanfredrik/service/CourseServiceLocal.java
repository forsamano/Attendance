package com.forsmanfredrik.service;

import com.forsmanfredrik.domain.Course;
import com.forsmanfredrik.domain.Student;

import javax.ejb.Local;
import java.util.List;
import java.util.Set;

/**
 * Created by ForsmanFredrik on 2015-09-14.
 */
@Local
public interface CourseServiceLocal {

    /**
     * adds a course to the database
     * @param course this course
     */
    void addCourse(Course course);

    /**
     * gets all courses in the database
     * @return all the courses in the database
     */
    List<Course> getCourses();

    /**
     * Finds the Course by it's id
     * @param id this course's id
     * @return this course
     */
    Course findCourseById(int id);

    /**
     * Removes the chosen course by it's id
     * @param id this course's id
     */
    void removeCourse(int id);

    /**
     * Edits the chosen course
     * @param course the chosen course
     * @return the course edited
     */
    Course editCourse(Course course);

    /**
     * Adds a student to a course by it's id
     * @param id this course's id
     * @param student the chosen student
     * @return a list of students in the course
     */
    Set<Student> addStudentToCourse(int id, Student student);

    /**
     * Removes a student from a course by it's id
     * @param courseId this course's id
     * @param student the chosen student
     * @return a list of students in the course
     */
    Set<Student> removeStudentFromCourse(int courseId, Student student);

    /**
     * gets all students in the chosen course
     * @param id this course's id
     * @return all students in the course
     */
    Set<Student> getStudentsInCourse(int id);

}
