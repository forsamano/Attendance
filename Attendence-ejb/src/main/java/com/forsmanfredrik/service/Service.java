package com.forsmanfredrik.service;

import javax.ejb.Stateful;
import javax.inject.Inject;

/**
 * Created by ForsmanFredrik on 2015-09-15.
 */
@Stateful
public class Service implements ServiceLocal {

    @Inject
    CourseServiceLocal courseServiceLocal;
    @Inject
    StudentServiceLocal studentServiceLocal;
    @Inject
    AttendanceServiceLocal attendanceServiceLocal;


    @Override
    public CourseServiceLocal getCourseService() {
        return courseServiceLocal;
    }

    @Override
    public StudentServiceLocal getStudentService() {
        return studentServiceLocal;
    }

    @Override
    public AttendanceServiceLocal getAttendanceService(){return attendanceServiceLocal;}

}
