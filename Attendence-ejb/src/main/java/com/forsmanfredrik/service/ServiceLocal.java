package com.forsmanfredrik.service;

/**
 * Created by ForsmanFredrik on 2015-09-15.
 */
public interface ServiceLocal {

    CourseServiceLocal getCourseService();

    StudentServiceLocal getStudentService();

    AttendanceServiceLocal getAttendanceService();

}
