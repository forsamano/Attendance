package com.forsmanfredrik.service;

import com.forsmanfredrik.domain.Student;

import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ForsmanFredrik on 2015-09-10.
 */
@Stateless(name = "PersonServiceEJB")
public class StudentServiceBean implements StudentServiceLocal {

    @PersistenceContext
    private EntityManager em;

    public StudentServiceBean() {
    }

    /**
     * {@inheritDoc}
     *
     */
    @Override
    public void addStudent(Student student) {
        em.persist(student);
        em.flush();
    }

    /**
     * {@inheritDoc}
     *
     */
    @Override
    public List<Student> getStudents() {

        TypedQuery<Student> query = em.createNamedQuery("Student.findAll", Student.class);
        List<Student> students = query.getResultList();

        return students;

    }

    /**
     * {@inheritDoc}
     *
     */
    @Override
    public Student findStudentById(int id) {

        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Student> query = cb.createQuery(Student.class);
            Root<Student> student = query.from(Student.class);
            query.where(cb.equal(student.get("id"), id));
            return em.createQuery(query).getSingleResult();
        } catch (NoResultException nre) {
            return null;
        } catch (NonUniqueResultException nur) {
            return null;
        }

    }

    /**
     * {@inheritDoc}
     *
     */
    @Override
    public Student editStudent(Student stud) {
        Student student = findStudentById(stud.getId());
        student.setFirstName(stud.getFirstName());
        student.setLastName(stud.getLastName());
        student.setCity(stud.getCity());
        student.setStreet(stud.getStreet());
        student.setZip(stud.getZip());
        student.setPhoneNumber(stud.getPhoneNumber());
        student.seteMail(stud.geteMail());

        return em.merge(student);
    }

    /**
     * {@inheritDoc}
     *
     */
    @Override
    public void removeStudentById(int id) {

        Student student = findStudentById(id);
        em.remove(student);
    }
}
