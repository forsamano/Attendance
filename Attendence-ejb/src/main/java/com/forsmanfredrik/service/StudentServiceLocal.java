package com.forsmanfredrik.service;

import com.forsmanfredrik.domain.Student;

import javax.ejb.Local;
import javax.persistence.EntityManager;
import java.util.List;

/**
 * Created by ForsmanFredrik on 2015-09-10.
 */
@Local
public interface StudentServiceLocal {

    /**
     * adds a student to the database
     * @param student this student
     */
    void addStudent(Student student);

    /**
     * gets all students in the database
     * @return all students
     */
    List<Student> getStudents();

    /**
     * finds the student by it's id
     * @param id this student's id
     * @return the chosen student
     */
    Student findStudentById(int id);

    /**
     * Removes the chosen student from the database by it's id
     * @param id this student's id
     */
    void removeStudentById(int id);

    /**
     * Edits the chosen Student
     * @param student the chosen student
     * @return the edited student
     */
    Student editStudent(Student student);

}
