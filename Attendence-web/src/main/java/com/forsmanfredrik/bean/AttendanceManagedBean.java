package com.forsmanfredrik.bean;

import com.forsmanfredrik.domain.Attendance;
import com.forsmanfredrik.domain.Student;
import com.forsmanfredrik.service.ServiceLocal;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.jms.JMSContext;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Created by ForsmanFredrik on 2015-10-05.
 */
@ManagedBean(name = "attendanceBean")
@ViewScoped
public class AttendanceManagedBean {

    public Attendance attendance;
    public Set<Student> students;
    public List<Attendance> attendances;
    private Date date;

    @Inject
    ServiceLocal serviceLocal;

    public Set<Student> getStudents() {
        return students;
    }

    public List<Attendance> getAttendances() {
        return getService().getAttendanceService().getAttendances();
    }

    @PostConstruct
    public void init(){
        attendance = new Attendance();
    }

    public Attendance getAttendance() {
        return attendance;
    }

    public void setAttendance(Attendance attendance) {
        this.attendance = attendance;
    }

    public ServiceLocal getService() {
        return serviceLocal;
    }

    public void setService(ServiceLocal serviceLocal) {
        this.serviceLocal = serviceLocal;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void addAttendance(){
        getService().getAttendanceService().addAttendance(attendance);
        attendance = new Attendance();
    }

    public Set<Student> addStudentToAttendance(int id, Student student){
       students = getService().getAttendanceService().addStudentToAttendance(id, student);
        return students;
    }

    public List<Attendance> getAttendanceByDate(Date date){
        return getService().getAttendanceService().getAttendancesByDate(date);
    }


}
