package com.forsmanfredrik.bean;

import com.forsmanfredrik.domain.Course;
import com.forsmanfredrik.domain.Student;
import com.forsmanfredrik.service.ServiceLocal;
import org.omnifaces.util.Ajax;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by ForsmanFredrik on 2015-09-14.
 */
@ManagedBean(name = "courseMB")
@ViewScoped
public class CourseManagedBean {

    private Course course;
    public List<Course> courses;
    public Set<Student> students;

    @Inject
    ServiceLocal serviceLocal;

    @PostConstruct
    public void init() {
        course = new Course();
    }

    public ServiceLocal getService() {
        return serviceLocal;
    }

    public void setService(ServiceLocal serviceLocal) {
        this.serviceLocal = serviceLocal;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public void addCourse() {
        getService().getCourseService().addCourse(this.course);
        course = new Course();
    }

    public Set<Student> addStudentToCourse(int id, Student student) {
        students = getService().getCourseService().addStudentToCourse(id, student);
        return students;
    }

    public Set<Student> removeStudentFromCourse(int courseId, Student student) {
        students = getService().getCourseService().removeStudentFromCourse(courseId, student);
        return students;
    }


    public void findCourse() {
        this.course = getService().getCourseService().findCourseById(this.course.getId());
    }

    public void editCourse() {
        getService().getCourseService().editCourse(this.course);
    }

    public void removeCourse(int id) {
        getService().getCourseService().removeCourse(id);
    }

    public void loadCourses() {
        this.courses = getService().getCourseService().getCourses();
    }

    public List<Course> getCourses() {

        return getService().getCourseService().getCourses();
    }

    public Set<Student> getStudentsInCourse(int id) {

        return getService().getCourseService().getStudentsInCourse(id);
    }

    public Set<Student> getStudents() {
        return students;
    }

    public void setStudents(Set<Student> students) {
        this.students = students;
    }

    public void setCourses(List<Course> courses) {
        this.courses = courses;
    }
}
