package com.forsmanfredrik.bean;

import com.forsmanfredrik.domain.Course;
import com.forsmanfredrik.domain.Student;
import com.forsmanfredrik.service.ServiceLocal;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ForsmanFredrik on 2015-09-10.
 */
@ManagedBean(name = "studentMB")
@ViewScoped
public class StudentManagedBean {

    private Student student;
    public List<Student> students;

    @Inject
    ServiceLocal serviceLocal;

    @PostConstruct
    public void init() {
        student = new Student();
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    public List<Student> getStudents() {

        return getService().getStudentService().getStudents();
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public ServiceLocal getService() {
        return serviceLocal;
    }

    public void setService(ServiceLocal serviceLocal) {
        this.serviceLocal = serviceLocal;
    }

    public void loadStudents() {
        this.students = getService().getStudentService().getStudents();
    }

    public void addStudent() {

        getService().getStudentService().addStudent(this.student);
        student = new Student();
    }

    public void clearStudent(){
        this.student = new Student();
    }

    public void findStudent() {
        this.student = getService().
                getStudentService().findStudentById(this.student.getId());
    }

    public void editStudent() {
        try {
            getService().getStudentService().editStudent(this.student);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void removeStudent(int id){
        getService().getStudentService().removeStudentById(id);
    }

}
